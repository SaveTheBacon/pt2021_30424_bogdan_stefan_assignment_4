import Business.Controller;

/**
 * Main class
 */
public class Main {
    /**
     * Instantiates the controller, starting the program.
     * @param args
     */
    public static void main(String[] args) {


        Controller controller = new Controller();
    }
}
