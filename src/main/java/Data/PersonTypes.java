package Data;

/**
 * the possible person types: admin, employee, client, as well as 2 auxiliary ones.
 */
public enum PersonTypes{
    ADMIN,
    EMPLOYEE,
    CLIENT,
    BAD,
    NONEXISTENT
}
