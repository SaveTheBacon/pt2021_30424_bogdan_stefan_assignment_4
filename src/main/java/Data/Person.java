package Data;

import java.io.Serializable;
import java.util.Objects;


/**
 * A user of our program.
 */
public class Person implements Serializable {


    private String name;
    private String pass;
    private PersonTypes type;

    /**
     * Instantiates a new Person.
     *
     * @param name the name
     * @param pass the pass
     */
    public Person(String name, String pass) {
        this.name = name;
        this.pass = pass;
        this.type = PersonTypes.CLIENT;
    }

    /**
     * Instantiates a new Person.
     *
     * @param name the name
     * @param pass the pass
     * @param type the type
     */
    public Person(String name, String pass, PersonTypes type) {
        this.name = name;
        this.pass = pass;
        this.type = type;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets password.
     *
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public PersonTypes getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        for (int i = 0; i < this.name.length(); i++) {
            hash = hash*31 + this.name.charAt(i);
        }
        return hash;
    }

    /*
        private void URGENTA(){
        ArrayList<Person> arrayList = new ArrayList<>();

        Person p1 = new Person("admin", "admin", PersonTypes.ADMIN);
        Person p2 = new Person("employee", "employee", PersonTypes.EMPLOYEE);
        Person p3 = new Person("client", "client", PersonTypes.CLIENT);
        arrayList.add(p1);
        arrayList.add(p2);
        arrayList.add(p3);

        AllUsers allUsers = new AllUsers(arrayList);

        Serialiser.serialiseUsers(allUsers);
        }

         */
}
