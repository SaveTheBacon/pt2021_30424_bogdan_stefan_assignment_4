package Data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class used to store all users that have been authenticated
 */
public class AllUsers implements Serializable {

    private ArrayList<Person> list;

    /**
     * Instantiates a new All users.
     *
     * @param list the list
     */
    public AllUsers(ArrayList<Person> list){
        this.list = list;
    }

    /**
     * Check existence of a person in the array list containing all users.
     *
     * @param person the person
     * @return the person types
     */
    public PersonTypes checkExistence(Person person){

        for (Person auxPer: list) {
            if(auxPer.getName().equals(person.getName())){
                if(auxPer.getPass().equals(person.getPass())){
                    return auxPer.getType();
                }
                return PersonTypes.BAD;
            }
        }

        return PersonTypes.NONEXISTENT;

    }

    /**
     * Add a new person.
     *
     * @param person the person
     */
    public void add(Person person){
        list.add(person);
    }
}
