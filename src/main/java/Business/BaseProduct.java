package Business;


import Data.Serialiser;

import java.io.Serializable;

/**
 * Base product, a single part of the menu item
 */
public class BaseProduct extends MenuItem implements Serializable {


    /**
     * Instantiates a new Base product.
     *
     * @param title    the title
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     */
    public BaseProduct(String title, Double rating, Integer calories, Integer protein, Integer fat, Integer sodium, Integer price) {
        super(title, rating, calories, protein, fat, sodium, price);
    }


    @Override
    public int computePrice() {
        return this.price;
    }

    @Override
    public Double computeRating() {
        return this.rating;
    }

    @Override
    public int computeCalories() {
        return this.calories;
    }

    @Override
    public int computeProtein() {
        return this.protein;
    }

    @Override
    public int computeFat() {
        return this.fat;
    }

    @Override
    public int computeSodium() {
        return this.sodium;
    }
}
