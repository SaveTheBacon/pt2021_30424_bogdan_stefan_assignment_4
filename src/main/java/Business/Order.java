package Business;

import Data.Person;
import Presentation.ClientGUI;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Class used to represent an order, placed by a customer
 */
public class Order implements Serializable {

    private Integer ID;
    private ArrayList<MenuItem> menuItems;
    private Person client;
    private GregorianCalendar gregorianCalendar;


    /**
     * Instantiates a new Order.
     *
     * @param ID                the id
     * @param menuItems         the menu items
     * @param client            the client
     * @param gregorianCalendar the gregorian calendar
     */
    public Order(Integer ID, ArrayList<MenuItem> menuItems, Person client, GregorianCalendar gregorianCalendar) {
        this.ID = ID;
        this.menuItems = menuItems;
        this.client = client;
        this.gregorianCalendar = gregorianCalendar;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getID() {
        return ID;
    }

    /**
     * Gets menu items.
     *
     * @return the menu items
     */
    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public Person getClient() {
        return client;
    }

    /**
     * Gets calendar, used to get the date+time of the order.
     *
     * @return the gregorian calendar
     */
    public GregorianCalendar getGregorianCalendar() {
        return gregorianCalendar;
    }

    /**
     * Verify if hour boolean lies between the bounds.
     *
     * @param startHour the start hour
     * @param endHour   the end hour
     * @return the boolean
     */
    public boolean verifyHour(Integer startHour, Integer endHour){
        if(this.gregorianCalendar.get(Calendar.HOUR_OF_DAY) > startHour && this.gregorianCalendar.get(Calendar.HOUR_OF_DAY) < endHour){
            return true;
        }
        else return false;

    }

    @Override
    public int hashCode(){
        MenuItem firstItem = menuItems.get(0);

        return (86017*ID + 86027*firstItem.getRating().intValue() + 86029* firstItem.getCalories() +
                86069* firstItem.getProtein() + 86077* firstItem.getFat() + 86083* firstItem.getSodium()
                +86161*firstItem.getPrice())%1260277;
    }
}
