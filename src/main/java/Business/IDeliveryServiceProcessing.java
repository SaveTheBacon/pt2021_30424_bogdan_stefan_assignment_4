package Business;


import Data.Person;
import Presentation.GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Interface containing all Delivery Service methods.
 */
public interface IDeliveryServiceProcessing {
     /**
      * Import product.
      * @pre fileName != null
      * @param fileName the file name
      */
     void importProduct(String fileName);

     /**
      * View products.
      *
      * @param tableModel the table model
      */
     void viewProducts(DefaultTableModel tableModel);

     /**
      * Add product.
      * @pre menuItem != null
      * @param menuItem the menu item
      */
     void addProduct(MenuItem menuItem);

     /**
      * Delete product.
      *
      * @pre name != null;
      * @post outNr >= -1
      * @param name the name of the product
      * @return the int
      */
     int deleteProduct(String name);

     /**
      * Edit product int.
      *@pre name != null;
      *@post outNr >= -1
      * @param menuItem the menu item
      * @return the int
      */
     int editProduct(MenuItem menuItem);

     /**
      * Generate reports for the administrator.
      *
      * @param startingHour               the starting hour
      * @param endingHour                 the ending hour
      * @param productAppearanceThreshold the minimum number of items that need to appear
      * @param clientOrdersThreshold      the minimum number of orders the client must place
      * @param orderValueThreshold        the minimum value of the client's order
      */
     void generateReport(Integer startingHour, Integer endingHour, Integer productAppearanceThreshold, Integer clientOrdersThreshold , Integer orderValueThreshold );

     /**
      * Create composite product.
      * @pre title != null
      * @post compProd != null
      * @param title the title
      * @return the composite product
      */
     CompositeProduct createCompProd(String title);

     /**
      * Generate order.
      * @pre currClient != null
      * @param currClient the curr client
      */
     void generateOrder(Person currClient);

     /**
      * Search the table, based on different parameters
      *
      * @param tableModel  the table model
      * @param greater     the boolean which tells us if we want values greater or smaller than the numeric value
      * @param productName the product name
      * @param field       the field to which the numeric value refers to
      * @param numericVal  the numeric value
      */
     void search(DefaultTableModel tableModel, boolean greater, String productName, String field, Number numericVal);
}
