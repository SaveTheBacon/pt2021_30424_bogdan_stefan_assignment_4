package Business;


import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Class used to represent an item from the restaurant menu
 */
public abstract class MenuItem implements Serializable {

    /**
     * The Title.
     */
    protected String title;
    /**
     * The Rating.
     */
    protected Double rating;
    /**
     * The Calories.
     */
    protected Integer calories;
    /**
     * The Protein.
     */
    protected Integer protein;
    /**
     * The Fat.
     */
    protected Integer fat;
    /**
     * The Sodium.
     */
    protected Integer sodium;
    /**
     * The Price.
     */
    protected Integer price;

    /**
     * Compute price.
     *
     * @return the int
     */
    public abstract int computePrice();

    /**
     * Compute rating.
     *
     * @return the double
     */
    public abstract Double computeRating();

    /**
     * Compute calories.
     *
     * @return the int
     */
    public abstract int computeCalories();

    /**
     * Compute protein.
     *
     * @return the int
     */
    public abstract int computeProtein();

    /**
     * Compute fat.
     *
     * @return the int
     */
    public abstract int computeFat();

    /**
     * Compute sodium.
     *
     * @return the int
     */
    public abstract int computeSodium();



    /**
     * Instantiates a new Menu item.
     */
    public MenuItem() {
    }

    /**
     * Instantiates a new Menu item.
     *
     * @param title    the title
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     */
    public MenuItem(String title, Double rating, Integer calories, Integer protein, Integer fat, Integer sodium, Integer price) {
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.protein = protein;
        this.fat = fat;
        this.sodium = sodium;
        this.price = price;
    }

    /**
     * fields to array. <br>
     *     takes the class fields, and puts them in a string array, to be used in the presentation layer.
     *
     * @return the string [ ]
     */
    public String[] fieldsToArray(){
        String[] row = new String[7];
        row[0] = this.getTitle();
        row[1] = this.getRating().toString();
        row[2] = this.getCalories().toString();
        row[3] = this.getProtein().toString();
        row[4] = this.getFat().toString();
        row[5] = this.getSodium().toString();
        row[6] = this.getPrice().toString();

        return row;

    }

    /**
     * Get location index. <br>
     *     searches for the index of a specific item from an arrayList, using the title for searching.
     *
     * @param arrayList the array list
     * @param title     the title
     * @return the index
     */
    public static Integer getLocationIndex(ArrayList<MenuItem> arrayList, String title){
        int i = 0;
        for (MenuItem menuItem: arrayList) {
            if(menuItem.getTitle().equals(title)){
                return i;
            }
            i++;
        }

        return -1;
    }

    /**
     * Get item from array list. <br>
     *     Takes an array list, extracts item with matching title.
     *
     * @param arrayList the array list
     * @param title     the title
     * @return the menu item
     */
    public static MenuItem getItemFromArrayList(ArrayList<MenuItem> arrayList, String title){
        for (MenuItem menuItem: arrayList) {
            if (menuItem.getTitle().equals(title)){
                return menuItem;
            }
        }
        return null;
    }

    /**
     * Get total price. <br>
     *     gets the total value of all menu items from an array list
     *
     * @param arrayList the array list
     * @return the int
     */
    public static int getTotalPrice(ArrayList<MenuItem> arrayList){
        int totalVal = 0;
        for (MenuItem menuItem: arrayList) {
            totalVal += menuItem.getPrice();
        }
        return totalVal;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets rating.
     *
     * @return the rating
     */
    public Double getRating() {
        return rating;
    }

    /**
     * Sets rating.
     *
     * @param rating the rating
     */
    public void setRating(Double rating) {
        this.rating = rating;
    }

    /**
     * Gets calories.
     *
     * @return the calories
     */
    public Integer getCalories() {
        return calories;
    }

    /**
     * Sets calories.
     *
     * @param calories the calories
     */
    public void setCalories(Integer calories) {
        this.calories = calories;
    }

    /**
     * Gets protein.
     *
     * @return the protein
     */
    public Integer getProtein() {
        return protein;
    }

    /**
     * Sets protein.
     *
     * @param protein the protein
     */
    public void setProtein(Integer protein) {
        this.protein = protein;
    }

    /**
     * Gets fat.
     *
     * @return the fat
     */
    public Integer getFat() {
        return fat;
    }

    /**
     * Sets fat.
     *
     * @param fat the fat
     */
    public void setFat(Integer fat) {
        this.fat = fat;
    }

    /**
     * Gets sodium.
     *
     * @return the sodium
     */
    public Integer getSodium() {
        return sodium;
    }

    /**
     * Sets sodium.
     *
     * @param sodium the sodium
     */
    public void setSodium(Integer sodium) {
        this.sodium = sodium;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menuItem = (MenuItem) o;
        return Objects.equals(title, menuItem.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
