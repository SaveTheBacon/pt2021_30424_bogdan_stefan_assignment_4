package Presentation;

import javax.swing.*;

/**
 * Client GUI.
 */
public class ClientGUI extends JFrame{
    private JPanel mainPanel;
    private JButton returnButton;
    private JTable table1;
    private JButton viewButton;
    private JButton searchButton;
    private JTextField nameTextField;
    private JTextField numericValueTextField;
    private JButton addButton;
    private JButton finaliseOrderButton;
    private JButton removeButton;
    private JComboBox greaterSmallerComboBox;
    private JComboBox fieldComboBox;
    private JComboBox orderComboBox;

    /**
     * Instantiates a new Client gui.
     */
    public ClientGUI(){
        add(mainPanel);
        setTitle("Client panel");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1000,600);
        setVisible(true);
    }

    /**
     * Gets return button.
     *
     * @return the return button
     */
    public JButton getReturnButton() {
        return returnButton;
    }

    /**
     * Gets table 1.
     *
     * @return the table 1
     */
    public JTable getTable1() {
        return table1;
    }

    /**
     * Gets view button.
     *
     * @return the view button
     */
    public JButton getViewButton() {
        return viewButton;
    }

    /**
     * Sets table 1.
     *
     * @param table1 the table 1
     */
    public void setTable1(JTable table1) {
        this.table1 = table1;
    }

    /**
     * Gets search button.
     *
     * @return the search button
     */
    public JButton getSearchButton() {
        return searchButton;
    }

    /**
     * Gets name text field.
     *
     * @return the name text field
     */
    public JTextField getNameTextField() {
        return nameTextField;
    }

    /**
     * Gets numeric value text field.
     *
     * @return the numeric value text field
     */
    public JTextField getNumericValueTextField() {
        return numericValueTextField;
    }

    /**
     * Gets add button.
     *
     * @return the add button
     */
    public JButton getAddButton() {
        return addButton;
    }

    /**
     * Gets finalise order button.
     *
     * @return the finalise order button
     */
    public JButton getFinaliseOrderButton() {
        return finaliseOrderButton;
    }

    /**
     * Gets remove button.
     *
     * @return the remove button
     */
    public JButton getRemoveButton() {
        return removeButton;
    }

    /**
     * Gets greater smaller combo box.
     *
     * @return the greater smaller combo box
     */
    public JComboBox getGreaterSmallerComboBox() {
        return greaterSmallerComboBox;
    }

    /**
     * Gets field combo box.
     *
     * @return the field combo box
     */
    public JComboBox getFieldComboBox() {
        return fieldComboBox;
    }

    /**
     * Gets order combo box.
     *
     * @return the order combo box
     */
    public JComboBox getOrderComboBox() {
        return orderComboBox;
    }
}
