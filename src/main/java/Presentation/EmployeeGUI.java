package Presentation;

import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

/**
 * Employee GUI.
 */
public class EmployeeGUI extends JFrame  {


    private JPanel mainPanel;
    private JTextField textField1;
    private JButton returnButton;
    private JButton clearNotificationButton;

    /**
     * Update.
     *
     * @param o   the o
     * @param arg the arg
     */
    public void update(Observable o, Object arg) {

    }

    /**
     * Instantiates a new Employee gui.
     */
    public EmployeeGUI(){
        add(mainPanel);
        setTitle("Employee panel");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600,600);
        setVisible(true);
    }

    /**
     * Gets text field 1.
     *
     * @return the text field 1
     */
    public JTextField getTextField1() {
        return textField1;
    }

    /**
     * Gets return button.
     *
     * @return the return button
     */
    public JButton getReturnButton() {
        return returnButton;
    }

    /**
     * Gets clear notification button.
     *
     * @return the clear notification button
     */
    public JButton getClearNotificationButton() {
        return clearNotificationButton;
    }
}
