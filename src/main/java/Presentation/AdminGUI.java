package Presentation;

import Data.Serialiser;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Admin GUI.
 */
public class AdminGUI extends JFrame {

    private JPanel mainPanel;
    private JButton returnButton;
    private JTable table1;
    private JButton viewButton;
    private JButton importButton;
    private JButton addButton;
    private JButton editButton;
    private JButton generateReportsButton;
    private JTextField ratingTextField;
    private JTextField nameTextField;
    private JTextField caloriesTextField;
    private JTextField proteinTextField;
    private JTextField fatTextField;
    private JTextField sodiumTextField;
    private JTextField priceTextField;
    private JTextField fileNameTextField;
    private JButton deleteButton;
    private JButton createNewProductButton;
    private JButton addButton1;
    private JButton removeButton;
    private JComboBox comboBox1;
    private JTextField newNameTextField;
    private JTextField startHTextField;
    private JTextField endHTextField;
    private JTextField productThreshTextField;
    private JTextField clientThreshTextField;
    private JTextField orderThreshTextField;


    /**
     * Instantiates a new Admin gui.
     */
    public AdminGUI() {
        add(mainPanel);
        setTitle("Admin panel");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1400,600);
        setVisible(true);
    }

    /**
     * Gets return button.
     *
     * @return the return button
     */
    public JButton getReturnButton() {
        return returnButton;
    }

    /**
     * Gets table 1.
     *
     * @return the table 1
     */
    public JTable getTable1() {
        return table1;
    }

    /**
     * Sets table 1.
     *
     * @param table1 the table 1
     */
    public void setTable1(JTable table1) {
        this.table1 = table1;
    }

    /**
     * Gets view button.
     *
     * @return the view button
     */
    public JButton getViewButton() {
        return viewButton;
    }

    /**
     * Gets import button.
     *
     * @return the import button
     */
    public JButton getImportButton() {
        return importButton;
    }

    /**
     * Gets add button.
     *
     * @return the add button
     */
    public JButton getAddButton() {
        return addButton;
    }

    /**
     * Gets edit button.
     *
     * @return the edit button
     */
    public JButton getEditButton() {
        return editButton;
    }

    /**
     * Gets generate reports button.
     *
     * @return the generate reports button
     */
    public JButton getGenerateReportsButton() {
        return generateReportsButton;
    }

    /**
     * Gets rating text field.
     *
     * @return the rating text field
     */
    public JTextField getRatingTextField() {
        return ratingTextField;
    }

    /**
     * Gets name text field.
     *
     * @return the name text field
     */
    public JTextField getNameTextField() {
        return nameTextField;
    }

    /**
     * Gets calories text field.
     *
     * @return the calories text field
     */
    public JTextField getCaloriesTextField() {
        return caloriesTextField;
    }

    /**
     * Gets protein text field.
     *
     * @return the protein text field
     */
    public JTextField getProteinTextField() {
        return proteinTextField;
    }

    /**
     * Gets fat text field.
     *
     * @return the fat text field
     */
    public JTextField getFatTextField() {
        return fatTextField;
    }

    /**
     * Gets sodium text field.
     *
     * @return the sodium text field
     */
    public JTextField getSodiumTextField() {
        return sodiumTextField;
    }

    /**
     * Gets price text field.
     *
     * @return the price text field
     */
    public JTextField getPriceTextField() {
        return priceTextField;
    }

    /**
     * Gets file name text field.
     *
     * @return the file name text field
     */
    public JTextField getFileNameTextField() {
        return fileNameTextField;
    }

    /**
     * Gets delete button.
     *
     * @return the delete button
     */
    public JButton getDeleteButton() {
        return deleteButton;
    }

    /**
     * Gets create new product button.
     *
     * @return the create new product button
     */
    public JButton getCreateNewProductButton() {
        return createNewProductButton;
    }

    /**
     * Gets add button 1.
     *
     * @return the add button 1
     */
    public JButton getAddButton1() {
        return addButton1;
    }

    /**
     * Gets remove button.
     *
     * @return the remove button
     */
    public JButton getRemoveButton() {
        return removeButton;
    }

    /**
     * Gets combo box 1.
     *
     * @return the combo box 1
     */
    public JComboBox getComboBox1() {
        return comboBox1;
    }

    /**
     * Gets new name text field.
     *
     * @return the new name text field
     */
    public JTextField getNewNameTextField() {
        return newNameTextField;
    }

    /**
     * Gets start h text field.
     *
     * @return the start h text field
     */
    public JTextField getStartHTextField() {
        return startHTextField;
    }

    /**
     * Gets end h text field.
     *
     * @return the end h text field
     */
    public JTextField getEndHTextField() {
        return endHTextField;
    }

    /**
     * Gets product thresh text field.
     *
     * @return the product thresh text field
     */
    public JTextField getProductThreshTextField() {
        return productThreshTextField;
    }

    /**
     * Gets client thresh text field.
     *
     * @return the client thresh text field
     */
    public JTextField getClientThreshTextField() {
        return clientThreshTextField;
    }

    /**
     * Gets order thresh text field.
     *
     * @return the order thresh text field
     */
    public JTextField getOrderThreshTextField() {
        return orderThreshTextField;
    }
}
