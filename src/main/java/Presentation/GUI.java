package Presentation;

import java.io.Serializable;

/**
 * Main GUI class, contains all sub-GUI's.
 */
public class GUI implements Serializable {

    private AdminGUI adminGUI;
    private ClientGUI clientGUI;
    private EmployeeGUI employeeGUI;
    private LogInGUI logInGUI;

    /**
     * Instantiates a new Gui.
     */
    public GUI(){
        this.adminGUI = new AdminGUI();
        this.clientGUI = new ClientGUI();
        this.employeeGUI = new EmployeeGUI();
        this.logInGUI = new LogInGUI();

        clientGUI.setVisible(false);
        adminGUI.setVisible(false);
        employeeGUI.setVisible(false);
        logInGUI.setVisible(true);
    }


    /**
     * Gets admin gui.
     *
     * @return the admin gui
     */
    public AdminGUI getAdminGUI() {
        return adminGUI;
    }

    /**
     * Gets client gui.
     *
     * @return the client gui
     */
    public ClientGUI getClientGUI() {
        return clientGUI;
    }

    /**
     * Gets employee gui.
     *
     * @return the employee gui
     */
    public EmployeeGUI getEmployeeGUI() {
        return employeeGUI;
    }

    /**
     * Gets log in gui.
     *
     * @return the log in gui
     */
    public LogInGUI getLogInGUI() {
        return logInGUI;
    }
}
